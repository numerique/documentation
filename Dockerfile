FROM node:latest
LABEL description="docsify builder"
WORKDIR /docs
RUN npm install -g docsify-cli@latest
EXPOSE 4000/tcp
ENTRYPOINT docsify serve . -p 4000