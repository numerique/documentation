# Documentation de la mission numérique


Bienvenue sur cette documentation. Vous y trouverez des informations sur les actions de la mission numérique qui n'ont pas encore été ajoutées aux documentations de la DSI.

Une documentation par outil existe déjà, vous pouvez les retrouver dans le menu de gauche.

## Table des matières

Ici, un petit résumé des actions de la mission numérique avec les liens vers les pages de cette documentation correspondantes.

### Automatisation des processus métier



