# Yousign

## Qualification des signatures

Yousign est conforme au réglement européen eIDAS mais pas au référentiel général de sécurité (RGS). 

Un sujet sur l'articulation RGS/eIDAS à l'UTC est lancé.


## Champs de signature (Smart Anchors)

Pour faire en sorte que la signature soit automatiquement placée à un endroit précis du document, il est possible d'utiliser les "Smart Anchors". Ces dernières sont des ancres de texte qui permettent de définir l'emplacement de la signature.

Il suffit de placer le texte suivant à l'endroit où vous souhaitez que la signature soit placée :

```
{{s1|signature|85|37}}
```

- `s1` : Numéro du signataire. Si vous avez plusieurs signataires, il faudra incrémenter ce numéro pour chaque signataire. Par exemple, `s2` pour le deuxième signataire, `s3` pour le troisième, etc.
- `signature` : Type de la signature, pour nos usages, il s'agit toujours de `signature`.
- `85` : La longueur de la signature en pixels. Le minimum est de 85 pixels et le maximum est de 580 pixels.
- `37`:  La hauteur de la signature en pixels. Le minimum est de 37 pixels et le maximum est de 253 pixels.

> :warning: Le texte de l'ancre sera visible dans le document généré. Pour rendre les ancres invisibles dans le document final, il faut faire en sorte que le texte de l'ancre corresponde à la couleur de fond (souvent blanc).

## Processus de signature par API

Toute la procédure est décrite dans la documentation de Yousign assez bien faite: https://developers.yousign.com/docs/notification-managed-by-yourself-1