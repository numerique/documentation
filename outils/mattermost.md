# Mattermost

## Contexte d'utilisation

Mattermost est l'outil de messagerie instantanée organisé par canaux, dont un instance est historiquement hébergé par Picasoft, ce qui fait qu'un nombre important d'UTCéens étudients et personnels l'utilisent.

Dans le cadre du projet "Communication Interne" du NUMexp, il a été déployé pour permettre aux membres de tester l'outil. Il n'est pas prévu de le déployer en production pour l'instant, il a donc été coupé. Il était accessible à l'adresse [https://chat.beta.utc.fr](https://chat.beta.utc.fr).

## Installation

Configuration docker-compose:

```yml
services:
  mattermost:
    # Etape de build obligatoire pour utiliser la version FOSS
    image: mattermost:9.6.1
    # Mettre à jour :
    # docker compose pull && docker compose build && docker compose up -d
    build:
      context: https://github.com/mattermost/mattermost.git#v9.6.1:server/build
      args:
        MM_PACKAGE: https://releases.mattermost.com/9.6.1/mattermost-team-9.6.1-linux-amd64.tar.gz
    container_name: mattermost
    security_opt:
      - no-new-privileges:true
    volumes:
      - mattermost-data:/mattermost/data
      - mattermost-plugins:/mattermost/plugins
      - /etc/localtime:/etc/localtime:ro
    env_file: ./secrets/mattermost-db.secrets
    networks:
      - proxy
      - mattermost
    # C'est obligatoire pour avoir accès au postfix local
    extra_hosts:
      - "host.docker.internal:host-gateway"
    depends_on:
      - mattermost-db
    ports:
      - 8443:8443/udp
      - 8443:8443/tcp
    restart: unless-stopped

  mattermost-db:
    image: postgres:15-alpine
    container_name: mattermost-db
    volumes:
      - mattermost-db:/var/lib/postgresql/data
      - /etc/localtime:/etc/localtime:ro
    env_file: ./secrets/mattermost-db.secrets
    networks:
      - mattermost
    restart: unless-stopped

volumes:
  mattermost-data:
    name: mattermost-data
  mattermost-plugins:
    name: mattermost-plugins
  mattermost-db:
    name: mattermost-db
```

Configuration reverse-proxy:
```conf
server {
    server_name chat.beta.utc.fr;

    listen 80;
    listen [::]:80;
    return 301 https://$host$request_uri;
}

server {
    server_name chat.beta.utc.fr;

    listen 443 ssl;      # for nginx v1.25.1+
    listen [::]:443 ssl; # for nginx v1.25.1+
	
    http2 on;

    location / {
        proxy_pass http://mattermost:8065;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header X-Forwarded-Scheme $scheme;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Accept-Encoding "";
        proxy_set_header Host $host;
    
        # proxy_read_timeout 86400s;
        client_body_buffer_size 512k;
        client_max_body_size 512m;

        # Websocket
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
    }

    ssl_certificate /ssl/live/process.utc.fr/fullchain.pem;   # managed by certbot on host machine
    ssl_certificate_key /ssl/live/process.utc.fr/privkey.pem; # managed by certbot on host machine

    ssl_session_timeout 1d;
    ssl_session_cache shared:MozSSL:10m; # about 40000 sessions
    ssl_session_tickets off;

    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-CHACHA20-POLY1305;
    ssl_prefer_server_ciphers on;
}

```


## Installation plugin Zoom

Pour installer le plugin Zoom, cliquer sur le bouton en haut à droit avec 9 carrés, puis "Marché d'applications":

![Marché d'applications](../img/mattermost-1.png)

Scroller ensuite jusqu'à trouver "Zoom" et cliquer sur "Installer":

![Installer Zoom](../img/mattermost-2.png)

Une fois installé, il faut configurer le plugin en cliquant sur "Configurer":

![Configurer Zoom](../img/mattermost-3.png)

Il faut ensuite entrer `https://utc-fr.zoom.us` dans le champ "Zoom URL", puis `https://api.zoom.us/v2` dans le champ "Zoom API URL".

Pour les champs `Zoom OAuth Client ID`, `Zoom OAuth Client Secret`, `At Rest Token Encryption Key`, `Webhook Secret` et `Zoom Webhook Secret`, il faut les générer sur le site de Zoom. Demander à la DSI.

![Configurer Zoom](../img/mattermost-4.png)


## Plugin Zoom: Un plugin pas adapté

Le plugin zoom, selon notre idée de de ce plugin, était censé permettre de lancer des réunions à l'interieur de canaux. Si par exemple, je décide de lancer une discussion puis que je m'en vais dans une autre, il ne faut donc pas arrêter la précédente discussion.

Zoom ne permet pas ça car au lieu de créer une réunion par canal, il utilise la salle de réunion personelle.

Si deux personnes lancent un réunion dans le même canal, ce sera donc deux salles qui seront créées.

![Problème Zoom](../img/mattermost-5.png)
