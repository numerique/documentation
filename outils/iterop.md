# Iterop

## Rôles et tarification

Il existe 5 rôles dans Iterop dont il est important de conaître les différences pour quantifier le nombre d'utilisateurs de chaque rôle. Et déterminer le coût de la licence.

1. Business Process Analyst
2. Business Process Designer
3. Business Process Player
4. Business Process Contributor
5. External User

### Rôles de gestion de processus

Les rôles 1 et 2 sont des rôles liés à la gestion des processus.
À l'UTC on imagine que le rôle d'analyste soit dédiés à la DPAC qui se servira de ces analyses pour améliorer e manière continue les processus modélisés. On peut donc imaginer qu'une seule personne soit nécessaire pour ce rôle.

Pour le design, on souhaite le faire en 2 étapes. La première étape consiste à modéliser les processus existants et à l'améliorer, c'est, là encore le rôle de la DPAC en collaboration avec le référent métier et le numérique. La seconde étape consiste à numériser le processus et à réaliser l'automatisation. On peut donc imaginer avoir une ou deux personne à la DPAC et 2 ou 3 à la DSI. 

Le rôle player est un prérequis pour les rôles de Designer et d'Analyst, on sait donc que pour la gestion des processus, il nous faut autant de joueurs que la somme des designers et des analystes.


Pour les rôles de gestion de processus on estime donc
1. Business Process Analyst: 1
2. Business Process Designer: 5
3. Business Process Player: 6

### Rôles d'exécution de processus

Les trois autres rôles participent à l'éxécution des processus.

Le rôle de joueur (Player) permet de réaliser des tâches, lancer des processus et suivre l'avancement de certains processus.

Le rôle de contributeur (Contributor) permet de réaliser des tâches et de lancer des processus mais pas de suivre l'avancement des processus.

Le rôle d'utilisateur externe (External User) permet de réaliser les tâches affectées à l'utilisateur uniquement sur invitation par mail. Il peut également lancer des processus via des fromulaires en ligne. La notion de processus est donc transparente pour l'utilisateur externe.

Déterminer le nombre de personnes ayant ces rôles se fait processus par processus. 

Pour le processus de Césure il y a plusieurs acteurs ou catégories d'acteurs:
- Les étudiants: Ils peuvent être considérés comme des utilisateurs externes ou comme des contributeur si on souhaite leur donner accès à la liste des processus
- Les responsables pédagogiques: Ils peuvent être considérés comme des contributeurs. En considérant 7 équivalent branches (GB, GI, GP, GU, HUTECH, IM, TC) et 2 responsables par branche, on peut estimer qu'il y a 14 contributeurs.
- Le coordinateur ingénieur UTC: Il peut être considéré comme un contributeur.
- Le DFP: Il peut être considéré comme un contributeur.
- Le SAE: Il peut être considéré comme un player car il doit suivre l'avancement de toutes les instances de processus de césure.

En tout on compte donc:
1. Business Process Player: 1
2. Business Process Contributor: 16
3. External User: 60/an avec 2 tâches donc 120 tâches/an.

### Tarification

Pour calculer le coût de la licence, on peut utiliser le script python suivant:

```python
############################################
# Paramètres du processus
############################################
# Nombre de licences
player = 1
contributor = 16

# Nombre d'instances annuelles de processus
nb_instances = 60

# Nombre de tâches
nb_external_or_contributor_human_tasks = 2
nb_api_tasks = 10
nb_mail_tasks = 4

############################################
# Coût des licences et des tâches
############################################
# Cout de la licence
player_price = 252
contributor_price = 12

# Nombre de crédit offert par licence
player_credit = 300
contributor_credit = 12

# Prix (en crédit) des tâches
human_task_price = 1
api_task_price = 0.1
mail_task_price = 0.01

############################################
# Calculs
############################################

# Total crédits offerts
total_credit = player * player_credit + contributor * contributor_credit

# Coût total en licence
total_price = player * player_price + contributor * contributor_price  

# Coût total en crédit
total_credit_price = nb_external_or_contributor_human_tasks * nb_instances * human_task_price + nb_api_tasks * nb_instances * api_task_price + nb_mail_tasks * nb_instances * mail_task_price

# Crédits restants
remaining_credit = total_credit - total_credit_price

# Afficher résultats
print(f"Cout annuel: {total_price}")
print(f"Crédits offerts: {total_credit}")
print(f"Crédits utilisés: {total_credit_price}")
print(f"Crédits restants: {remaining_credit}")
```




## Étude comparative iterop camunda

### Constructeurs de formulaires

Ces notes sont basées sur la construction du formulaire de demande de césure des deux outils et écrites pendant la réalisation avec Iterop.

Les premiers champs textes représentent la même difficulté.

Pour les champs en liste de valeur tels que les branches, les niveau et les semestres, elle ont l'avantage d'être centralisée. Sur Camunda, la liste des semestres est générée dans le formulaire.
Sur Iterop, le formulaire appelle une liste de valeurs qui est définie dans un autre onglet. Ainsi un autre process est responsable de mettre à jour deux fois par jour la liste des semestres de départ autorisé. La logique de calcul et les deadlines sont donc indépendantes du formulaire et donc réutilisable. [Voir la documentation du process de mise à jour des listes de valeurs](#process-de-mise-à-jour-des-listes-de-valeurs).

### Process de mise à jour des listes de valeurs

Pour mettre à jour la liste de valeur, un process comme suit est mis en place:

![Process de mise à jour des listes de valeurs](../img/process_maj_liste_valeurs.png)

Le process est déclenché par un timer qui se déclenche deux fois par jour. Il lance un script qui va chercher les valeurs actuelles et vide puis remplit la liste de valeurs. avec le code suivant:


```javascript
var list_id = 3;

// Trying not to clear the whole list
// But it doesn't display elements in the right order
// var old_semesters_string = list.getElements(list_id, ",");
// var old_semesters = old_semesters_string.split(",");

// Clear the list
list.clearList(list_id);

// Calculer les 4 prochains semestres
function getNextSemester(semester) {
  // Get P or A
  var prefix = semester[0];
  // Get the year
  var year = parseInt(semester.slice(1));

  // Si printemps, le prochain semestre est l'automne de la même année
  if (prefix === "P") {
    return "A" + year;
  }

  // Si automne, le prochain semestre est le printemps de l'année suivante
  return "P" + (year + 1);
  
}

// ES6 syntax
// function getDepartureSemesters(month = null, year = null) {
// ES5 syntax
function getDepartureSemesters(month, year) {

  var now = new Date();
  var currentMonth = month || now.getMonth() + 1;
  var currentYear = year || now.getFullYear();

  // Première condition
  var firstValue = ""

  // Si le mois est entre janvier et mai inclus
  // On peut partir en automne de cette année
  // Ou si le mois est décembre, on peut partir en automne de l'année suivante
  // Sinon, on peut partir au printemps de l'année suivante
  if (currentMonth >= 1 && currentMonth <= 5 || currentMonth === 12) {
    firstValue = "A"
    if (month === 12) {
      currentYear += 1
    }
    firstValue += String(currentYear).slice(2)
  } else {
    firstValue = "P"
    firstValue += String(currentYear + 1).slice(2)
  }

  var semesters = [firstValue];
  for (var i = 0; i < 3; i++) {
    semesters.push(getNextSemester(semesters[i]));
  }

  return semesters;
}


var semesters = getDepartureSemesters();


// Trying not to clear the whole list
// But it doesn't display elements in the right order
// // Suppression des éléments
// list.deleteElements(list_id, arrayDifference(old_semesters, semesters));

// // Ajout des nouveaux éléments
// list.addElements(list_id, arrayDifference(semesters, old_semesters));


// Fill the list
list.addElements(list_id, semesters);
```

Ce code est disponible sur le repository [iterop-sources](https://gitlab.utc.fr/numerique/iterop-sources) dans le dossier `scripts`.

L'utilisation de javascript est un avantage pour les personnes qui connaissent déjà ce langage, cependant, Iterop utilise le standard ECMAScript 5.1. Pour ceux qui ont l'habitude de coder en ECMAScript 6, il faudra donc adapter son code, ce qui peut être déroutant. Quelques exemples sont donnés dans le code ci-dessus.

Autre avantage de l'utilisation de javascript est qu'il est possible d'exécuter les fonctions en dehors de l'outil. Cela permet de tester le code et de le débugger plus facilement, par exemple la fonction qui génère les semestres de départ autorisés est isolée et testée aux limites dans le même repository que le script, cette fois ci dans le dossier `lib`. Le code ci dessous illustre comment tester la fonction `getDepartureSemesters`. Il suffit de copier coller dans la console de son navigateur. C'est donc un avantage considérable pour la qualité du code par rapport à Camunda.:

```javascript
function getNextSemester(semester) {
  // Voir code source
}

// ES6 syntax
// function getDepartureSemesters(month = null, year = null) {
// ES5 syntax
function getDepartureSemesters(month, year) {
    // Voir code source
}

function arraysAreEqual(arr1, arr2) {
  // Vérifier si les tableaux ont la même longueur
  if (arr1.length !== arr2.length) {
    return false;
  }
  
  // Vérifier chaque élément
  return arr1.every(function(value, index) {
    return value === arr2[index];
  });
}

// Unit test for getDepartureSemesters
// Compare the result of the function with the expected value

// console.log(getDepartureSemesters(6, 2021)); // true
console.log(arraysAreEqual(getDepartureSemesters(6, 2021), ["P22", "A22", "P23", "A23"])); // true
// Arra
console.log(getDepartureSemesters(12, 2021)); // true
console.log(arraysAreEqual(getDepartureSemesters(12, 2021), ["A22", "P23", "A23", "P24"])); // true

// console.log(getDepartureSemesters(1, 2022)); // true
console.log(arraysAreEqual(getDepartureSemesters(1, 2022), ["A22", "P23", "A23", "P24"])); // true

// console.log(getDepartureSemesters(5, 2022)); // true
console.log(arraysAreEqual(getDepartureSemesters(5, 2022), ["A22", "P23", "A23", "P24"])); // true
```

> Pour l'anecdocte, le code ci-dessus a permis de trouver un bug dans l'algorithme de calcul des semestres de départ autorisés. En effet, ne pouvant tester sur Camunda, le code ne générait pas les bons semestres si on était en décembre. Sur Camunda, on se serait rendu compte de l'erreur en décembre 2024.

La documentation des fonctionnalités spécifiques à Iterop est disponible sur l'outil et est assez complète. En déroulant le menu suivant:
![Documentation des fonctions spécifiques à Iterop](../img/iterop_doc.png)




