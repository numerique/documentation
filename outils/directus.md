# Directus

Directus est un outil permettant de réaliser des backends avec API et interface d'administration dans une logique low-code voir no-code.

Il a été installé dans le cadre du projet BPA. L'outil semble intéressant mais pas pour le projet BPA. Il ne semble pas avoir sa place, pour le moment dans le SI de l'UTC, il a donc été coupé et était acessible à l'adresse [https://make.beta.utc.fr](https://make.beta.utc.fr).

## Installation

Configuration docker-compose:

```yml
services:
    # Directus
    directus:
        image: directus/directus:10.10.5
        networks:
        - proxy
        - directus
        volumes:
        - directus-uploads:/directus/uploads
        - directus-extensions:/directus/extensions
        env_file:
        - ./secrets/directus-app.secrets
        - ./secrets/directus-db.secrets
        environment:
        DB_CLIENT: "postgres"
        DB_HOST: "directus-db"
        DB_DATABASE: "directus"
        DB_USER: "directus"
        DB_PORT: 5432
        WEBSOCKETS_ENABLED: "true"
        EMAIL_FROM: noreply@utc.fr
        EMAIL_TRANSPORT: "smtp"
        EMAIL_SMTP_HOST: "host.docker.internal"
        EMAIL_SMTP_PORT: 25
        EMAIL_SMTP_SECURE: false
        EMAIL_SMTP_IGNORE_TLS: true
        extra_hosts:
        - "host.docker.internal:host-gateway"
        restart: unless-stopped

    directus-db:
        image: postgres:15-alpine
        container_name: directus-db
        volumes:
        - directus-db:/var/lib/postgresql/data
        - /etc/localtime:/etc/localtime:ro
        env_file:
        - ./secrets/directus-db.secrets
        environment:
        POSTGRES_USER: "directus"
        networks:
        - directus
        restart: unless-stopped
volumes:
    # Directus
    directus-uploads:
        name: directus-uploads
    directus-extensions:
        name: directus-extensions
    directus-db:
        name: directus-db
```

Configuration reverse-proxy:
```conf
server {
    server_name make.beta.utc.fr;

    listen 80;
    listen [::]:80;
    return 301 https://$host$request_uri;
}

server {
    server_name make.beta.utc.fr;

    listen 443 ssl;      # for nginx v1.25.1+
    listen [::]:443 ssl; # for nginx v1.25.1+
	
    http2 on;

    location / {
        proxy_pass http://directus:8055;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header X-Forwarded-Scheme $scheme;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Accept-Encoding "";
        proxy_set_header Host $host;
    
        # proxy_read_timeout 86400s;
        client_body_buffer_size 512k;
        client_max_body_size 512m;

        # Websocket
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
    }

    ssl_certificate /ssl/live/process.utc.fr/fullchain.pem;
    ssl_certificate_key /ssl/live/process.utc.fr/privkey.pem;

    ssl_session_timeout 1d;
    ssl_session_cache shared:MozSSL:10m; # about 40000 sessions
    ssl_session_tickets off;

    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-CHACHA20-POLY1305;
    ssl_prefer_server_ciphers on;
}
```