# Docsify

Docsify est l'outil qui génère cette documentation. Il permet de générer une documentation à partir de fichiers markdown. Et permet ainsi de réaliser des documentations collaboratives en se basant sur git.

Un déploiement continu peut également se mettre en place. Cela permet de déployer automatiquement les modifications apportées à la documentation.

Pour des documentations techniques, c'est l'outil idéal. Il permet de structurer la documentation en plusieurs niveaux de profondeur. Il permet également de générer des liens entre les différentes pages de la documentation.

## Exemple

Un exemple est disponible sur gitlab: https://gitlab.utc.fr/numerique/documentation-exemple

### Prérequis

- Docker (avec docker-compose)
- Git

### Installation

À partir du repository gitlab, il suffit de cloner le repository et de lancer le serveur docsify.

```bash
git clone https://gitlab.utc.fr/numerique/documentation-exemple.git
cd documentation-exemple
docker compose up -d
```

La doc est ensuite acessible à l'adresse http://localhost:3001, pour l'éditer, il suffit de modifier les fichiers markdown et de rafraichir la page web.

## Documentation officielle

La documentation officielle de docsify est disponible à l'adresse suivante: https://docsify.js.org/#/

On y voit quelques points qui peuvent intéresser tout rédacteur d'une documentation:
- Redimensionner les images: https://docsify.js.org/#/helpers?id=image
- Ajouter le support pour la coloration syntaxique d'un nouveau langage: https://docsify.js.org/#/language-highlight?id=language-highlighting

