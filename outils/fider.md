# Fider

Fider est un outil permettant de remonter des feedbacks ou de voter pour les feedbacks les plus intéressant. Il est alors possible de connaitre ce qui est le plus attendu et ce qu'il faut prioriser.

Il s'agit de la version numérique d'un boite à idée. L'outil est hébergé sur https://feedbacks.utc.fr par la DSI. Le contact technique est [Dominique Chambelant](mailto:dominique.chambelant@utc.fr).

## Exemple d'utilisation

### Casiers connectés du CROUS au CI

Il s'agit d'un expérimentation lancée par la DPL, ces casiers sont achetés par l'UTC et le CROUS se charge de mettre à disposition des produits à l'intérieur.

Les étudiants peuvent alors récupérer des produits moyenant un paiement via carte bancaire.

Pour récupérer les retours, une affiche avec un QR code est placée sur les casiers. Les étudiants peuvent alors scanner le QR code et donner leur avis sur le service via Fider.

> **Attention** : l'usage des QRCode peut comporter des risques. Avant de générer un QR code, veuillez lire le [guide sur les QR Codes](/outils/qrcode.md).

## Évolutions de l'outil

La mission numérique a identifié une évolution importante de l'outil fider: La possibilité d'ajouter un tag à un feedback lors de la création.

Actuellement, c'est aux collaborateurs d'ajouter des tags et de les associer à un feedback. L'idéé étant de permettre aux utilisateurs de le faire directement lors de la création du feedback.

Ceci permettrai notamment de lancer des automatisation à l'aide de n8n pour notifier les personnes concernées par un feedback.

Par exemple sur le tag `Casiers Connectés`, on souhaite notifier la DPL et le CROUS.

Pour ce faire, une TX a été lancée par la mission numérique sur le semestre A24 pour contribuer au projet open source. Les travaux de cette TX seront publiés sur cette documentation.

## Mettre en place un feedback physique

Un template (ou modèle) a été créé par la com pour vous permettre de mettre en place un feedback physique.

Lorsqu'il sera finalisé, un lien sera mis à disposition pour le télécharger.

L'idée est de vous proposer de mettre un titre, un texte incitant à donner un feedback et un QR code pour rediriger vers Fider.

> **Attention** : l'usage des QRCode peut comporter des risques. Avant de générer un QR code, veuillez lire le [guide sur les QR Codes](/outils/qrcode.md).