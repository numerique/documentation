# Documentation carbone

Carbone est un outil de génération de documents PDF à partir de modèles. Il permet de générer des documents à partir de données structurées. Il est utilisé pour générer des documents de synthèse de données.

Dans le recherche d'autonomie des utilisateurs, carbone permet générer des documents pdf à partir de modèles word ou Open Document Text (ODT) sans passer par un développeur. Ainsi on peut provisionner un processus à partir d'un modèle de document hébergé sur le cloud UTC et accessible par une URL.

Voici une illustration tirée de Carbone:

![Carbone](../img/carbone-explained.png)

Voici le lien vers la documentation complète de carbone: [https://carbone.io/documentation.html](https://carbone.io/documentation.html) 

## Use case: Génération de rapport de césure à partir de données structurées

Avec le modèle word disponible au téléchargement [ici](/documents/cesure-template.docx ':ignore'), on peut générer un rapport de césure à partir de données structurées. 

Etape 1:Uploadez le modèle word sur carbone. Pour cela, effectuer une reqûete POST sur l'API de carbone à l'aide de la route `/api/template` avec le modèle word en tant que fichier.

> :warning: **Attention**: Parfois, il est nécessaire d'envoyer le fichier en base64. Pour cela, il faut encoder le fichier en base64 et l'envoyer dans le corps de la requête.
> Pour encoder un fichier en base64, vous pouvez utiliser la commande suivante:
> ```bash
> base64 -i fichier.docx -o fichier.base64
> ```

Etape 2: Générer le rapport de césure à partir des données structurées. Pour cela, effectuer une requête POST sur l'API de carbone à l'aide de la route `/api/render/{templateid}` avec les données structurées en tant que fichier JSON.

`templateid` est l'identifiant du modèle word que vous avez uploadé à l'étape 1 et qui est retourné dans la réponse de la requête POST sur `/api/template`.

Exemple de données structurées:

```json
{
  "data": {
    "nom": "Dupont",
    "prenom": "Jean",
    "semestre": "P24",
    "branche": "GU",
    "niveau": "02",
    "type_semestre_n": "SP",
    "type_semestre_n_1": "CE",
    "type_semestre_n_2": "SP",
    "lieu_semestre_n": "Paris",
    "lieu_semestre_n_1": "Pérou",
    "lieu_semestre_n_2": "Berlin",
    // ...
    // Autres variables
  },
  "convertTo": "pdf"
}
```

Cette route retourne un nom de fichier qui est le nom du fichier généré dans `data.renderId`. Pour télécharger le fichier généré, effectuer une requête GET sur la route `/api/render/{renderId}`.

## Comment héberger carbone

Une image docker est disponible pour héberger Carbone cloud on premise. La documentation d'installation est bien cachée, ce qui signifie que ce n'est pas une solution supporté par carbone et qu'elle n'est pas la priorité dans le business model de carbone. Le Dockerfile n'est pas open source, ce qui signifie que l'on ne peut pas le modifier pour l'adapter à nos besoins.

Voici le lien pour l'installation de carbone on premise: [https://carbone.io/on-premise.html#docker-cli-install](https://carbone.io/on-premise.html#docker-cli-install)

On en déduit la configuration suivante pour le fichier `docker-compose.yml`:

```yaml
version: '3'
services:
    carbone:
        image: carbone/carbone-ee:latest
        restart: always
        platform: linux/amd64
        volumes:
        - carbone-templates:/app/template
        - carbone-renders:/app/render
```

Les volumes `carbone-templates` et `carbone-renders` sont des volumes docker qui permettent de stocker les modèles de documents et les documents générés respectivement.

Le tout derrière un reverse proxy dont la configuration est la suivante:

```nginx
server {
	listen 80;
	server_name carbone.beta.utc.fr;

        location /.well-known/acme-challenge/ {
                root /acme-challenge;
        }



	location / {
		return 301 https://carbone.utc.fr$request_uri;
	}
}

server {

        listen 443 ssl;
	http2 on;
        server_name carbone.beta.utc.fr;

        location /.well-known/acme-challenge/ {
                root /acme-challenge;
        }


        ssl_certificate /ssl/live/process.utc.fr/fullchain.pem;
        ssl_certificate_key /ssl/live/process.utc.fr/privkey.pem;

        location / {

		auth_basic             "Restricted access";
		auth_basic_user_file   ./auth/carbone.passwd;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;

                # the real magic is here where we forward requests to the address that the Node.js server is running on
                proxy_pass http://carbone:4000;
        }
}
```