# Introduction

Cette documentation vise les *actuer* des processus automatisés. Les initiateurs sont les personnes auxquelles des tâches sont assignées. Ils doivent réaliser des actions pour faire avancer le processus.

## Liste des tâches et assignation

La liste des tâches est disponible sur [https://tasks.beta.utc.fr/](https://tasks.beta.utc.fr/). Cette interface permet de voir les tâches qui vous sont assignées et de les réaliser.

Certaines tâche de vous sont pas assignées directement mais peuvent être assignées à votre service. Dans ce cas, vous pouvez les voir et vous les assigner si vous êtes en charge de les réaliser.

## Réaliser une tâche

Lorsque vous vous cliquez sur un tâche, vous avez accès à toutes les informations nécessaires pour la réaliser puis à un formulaire pour entrer les informations nécessaires (Décision, avis, etc).

Par exemple pour le processus test de demande de césure, vous pouvez avoir à donner un avis sur la demande de césure. Ci dessous un exemple de formulaire déstiné au responsable pédagogique d'un étudiant pour donner son avis sur la demande de césure de l'étudiant.

![Formulaire de demande de césure](../img/formulaire_cesure.png)

Une fois la tâche validée, le processus avance et la tâche suivante est assignée à un autre acteur.

Le plus souvent, les infos de contact de l'initiateur et des autres acteurs sont disponibles dans le formulaire. Si vous avez besoin de plus d'informations, vous pouvez les contacter directement.