# Automatisation des processus métiers

## Introduction

L'automatisation des processus métiers à pour but de simplifier et d'optimiser les tâches répétitives et chronophages. Elle permet de réduire les erreurs humaines et d'augmenter la productivité.

L'idée est de permettre à chacun de dégager du temps pour les tâches à plus forte valeur ajoutée. Par exemple en automatisant les cas courants et en se concentrant sur les cas particuliers.

## Structuration de la documentation

On identifie 2 grandes catégories de rôles dans l'automatisation des processus métiers:
1. **Les utilisateurs finaux**
    1. **Les initiateurs**: les personnes qui déclenchent les processus automatisés
    2. **Les acteurs des processus**: les personnes qui interviennent dans les processus automatisés
2. **Les créateurs**
    1. **Les clients**: les personnes qui expriment le besoin d'automatisation
    2. **Les modélisateurs**: les personnes qui aident à la modélisation des processus
    3. **Les développeurs**: les personnes qui développent les processus automatisés

Une documentation est donc accessible pour chacun de ces rôles.


    
