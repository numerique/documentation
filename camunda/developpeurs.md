# Intro

## Lier un évènement "Start" à un formulaire

Lier un évènement "Start" à un formulaire permet à l'initiateur d'entrer des informations.

Il n'est cependant pas possible de lier un évènement start à un formulaire directement. Il faut passer par le xml généré.

Prenons l'exemple d'un fichier avec juste un évènement start.

Voici sa représentation graphique:
![StartEvent_Exemple](../../img/start_seul.png)

Et voici sa représentation xml:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:modeler="http://camunda.org/schema/modeler/1.0" id="Definitions_0o0wckr" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="5.24.0" modeler:executionPlatform="Camunda Cloud" modeler:executionPlatformVersion="8.5.0">
  <bpmn:process id="Process_Exemple" name="Exemple" isExecutable="true">
    <bpmn:startEvent id="StartEvent_Exemple" />
  </bpmn:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_Exemple">
      <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_Exemple">
        <dc:Bounds x="152" y="82" width="36" height="36" />
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn:definitions>
```

Il suffit de séparer la balise auto-fermante `bpmn:startEvent` en deux balises `bpmn:startEvent` pour encapsuler la balise `zeebe:formDefinition` comme ceci:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:modeler="http://camunda.org/schema/modeler/1.0" id="Definitions_0o0wckr" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="5.24.0" modeler:executionPlatform="Camunda Cloud" modeler:executionPlatformVersion="8.5.0">
  <bpmn:process id="Process_Exemple" name="Exemple" isExecutable="true">
    <bpmn:startEvent id="StartEvent_Exemple">
        <zeebe:formDefinition formId="Form_Exemple" />
    </bpmn:startEvent>
  </bpmn:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_Exemple">
      <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_Exemple">
        <dc:Bounds x="152" y="82" width="36" height="36" />
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn:definitions>
```

## Utiliser un connecteur REST pour récupérer des données

Prérequis:
- Savoir effectuer une requête HTTP à l'aide d'un outil comme Postman ou Curl.

Il est possible d'utiliser un connecteur REST pour récupérer des données depuis des API web.

Avant, il faut vérifier que le connecteur REST est installé sur l'instance de Camunda que vous utilisez et sur votre Machine avec le Camunda Modeler.

La liste des connecteurs installés sur le serveur de bêta UTC est disponible ici: [https://github.com/camunda/connectors/tree/main/connectors](https://github.com/camunda/connectors/tree/main/connectors).

Pour le camunda modeler, il faut ajouter la description des champs (template) dans le dossier `resources/element-templates`de Camunda, il est situé à des endroits différents selon les systèmes d'exploitation.

Plus d'infos ci dessous ou [ici](https://docs.camunda.io/docs/components/modeler/desktop-modeler/search-paths/#user-data-directory)

**Windows**
```
└── AppData
    └── Roaming
        └── camunda-modeler
            └── resources
                ├── element-templates
                |   └── my-element-templates.json
```

**MacOS**

```
└── Library
    └── Application Support
        └── camunda-modeler
            └── resources
                ├── element-templates
                |   └── my-element-templates.json
```

Un fois le connecteur installé, il suffit de l'ajouter à un service task dans le Camunda Modeler et de configurer les paramètres classiques d'une requête HTTP.