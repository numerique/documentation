# Introduction

Cette documentation vise les *initiateurs* des processus automatisés. Les initiateurs sont les personnes qui déclenchent les processus automatisés.

## Déclencher un processus

Pour déclencher un processus, il faut se rendre sur l'interface de Camunda. L'adresse de l'interface de Camunda est disponible sur [https://tasks.beta.utc.fr/](https://tasks.beta.utc.fr/).

Cet outil permet d'avoir la liste de tout les processus disponibles et de les déclencher. Deux modes de déclenchement sont possibles:
- **Sans formulaire**: le processus est déclenché sans avoir besoin d'entrer des infos supplémentaires. Cela veut dire que nous avons récupéré le login et que toutes les informations nécessaires sont déjà présentes dans les systèmes d'information.
- **Avec formulaire**: le processus est déclenché avec un formulaire à remplir. Cela veut dire que nous avons besoin d'informations supplémentaires pour déclencher le processus. Par exemple pour une demande de césure, un étudiant doit donner plus d'infos sur son projet.

Seule la liste des processus disponibles selon votre rôle est accessible. Par exemple si vous êtes un personnel, vous ne pouvez pas voir le processus de  demande de césure.

![Liste des processus](../img/liste_processus.png)

## Suivre son processus

Il n'existe, pour le moment pas d'interface permettant à un initiateur de suivre son processus. Cependant, selon les processus, vous recevrez des informations sur l'avancement de votre processus par mail. A terme, les informations d'avancement seront disponibles via le cloud.

Il est possible qu'un acteur du processus ai besoin de plus d'informations. Si c'est une procédure prévue, vous aurez besoin de compléter un formulaire, vous devenez alors acteur du processus. Si ce n'est pas prévu, vous serez contacté par mail. 

Voir la documentation **acteur de processus**.