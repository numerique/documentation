# Documentation du cloud bêta

Lien: [https://cloud.beta.utc.fr](https://cloud.beta.utc.fr)

Pour créer son compte, cliquer sur "S'inscrire". Ou cliquer [ici](https://cloud.beta.utc.fr/apps/registration/).
Un formulaire vous permet d'entrer votre login utc et de choisir quelles catégories d'actualités vous souhaitez suivre.

Pour les étudiants, vous serez ajoutés automatiquement à un canal "Actus BDE" dont le modérateur est "Roger Delacom" qui est administré par le BDE.

Pour les membres du NUMexp, vous êtes ajoutés automatiquement à un canal "NUMexp - Privé" nous permettant de discuter en cercle restreint.

Il s'agit là d'une base de travail, destinées à être améliorée au fur et à mesure.

## Nextcloud Talk sur Mobile

Pour utiliser nextcloud sur mobile, il y a deux applications: Une pour les fichiers et une autre pour le chat. On s'intéressera ici à l'application de chat.

Pour avoir Talk sur mobile, il faut télécharger l'application "Nextcloud Talk". Pour ce faire, sur android l'application est disponible sur le Play Store en cliquant sur [ce lien](https://play.google.com/store/apps/details?id=com.nextcloud.talk2&hl=fr). Sur IOS (iPhone et iPad), l'application est disponible sur l'App Store en cliquant sur [ce lien](https://apps.apple.com/fr/app/nextcloud-talk/id1125420102).

Ouvrir l'application.

![Ouvrir l'application](../img/nextcloud-mobile-1.jpg ':size=250')

Une fois sur l'application, il faut entrer `https://cloud.beta.utc.fr` dans le champ "Adresse du serveur https://..." et cliquer sur valider ou sur la flèche à droite du champs.

![Entrer l'adresse du serveur](../img/nextcloud-mobile-2.jpg ':size=250')

Une fois l'adresse URL entrée, il faut ensuite cliquer sur "Se connecter", une nouvelle page s'ouvre.

![Se connecter](../img/nextcloud-mobile-3.jpg ':size=250')
<!-- Divide size by 2 -->


Entrer votre login et votre mot de passe. Cliquer sur "Se connecter".

Vous aurez alors accès à vos fichiers et à vos conversations.

<!-- Warning -->
> **Attention**: Vous allez recevoir des notifications push, veuillez les désactiver si vous ne souhaitez pas être dérangé.Les canaux d'information sont réglés sur mute par défaut.

## Nextcloud Talk sur PC

Si vous êtes sur un PC, nous conseillons d'utiliser le navigateur web pour accéder à Nextcloud Talk. Cela permet d'avoir une meilleure expérience utilisateur.

<!-- Warning -->
> **Attention**: L'application bureau de Nextcloud Talk n'est pas encore très mûre notamment au niveau de l'installation et des mises à jours.

Néanmoins, si vous souhaitez avoir une application bureau, vous pouvez télécharger la dernière version [ici](https://github.com/nextcloud-releases/talk-desktop/releases/latest).

Ensuite dans "Assets", télécharger le fichier correspondant à votre système d'exploitation. Le fichier contenant `win32` est pour Windows, celui contenant `darwin` est pour OSX (Mac) et celui contenant `linux` est pour Linux.

### Windows

Un fois le téléchargement effectué, se rendre dans le dossier "Téléchargements" puis clic droit > "Extraire tout"

Vous pouvez ensuite déplacer le dossier extrait dans le dossier de votre choix puis créer un raccourci sur le bureau.

Ensuite lancer l'application en double cliquant sur le fichier `Nextcloud Talk.exe`.

Il est possible que Windows affiche un message d'avertissement, cliquer sur "Informations complémentaires".

![Informations complémentaires](../img/nextcloud-windows-1.png ':size=500')

Ensuite cliquer sur "Exécuter quand même".

![Exécuter quand même](../img/nextcloud-windows-2.png ':size=500')

Ensuite, entrer l'url du serveur `https://cloud.beta.utc.fr` et cliquer sur "Se connecter".

![Se connecter](../img/nextcloud-windows-3.png ':size=500')

On vous demande ensuite de valider le fait que vous voulez connecter votre appareil à votre compte. Cliquer sur "Se connecter".

![Se connecter](../img/nextcloud-windows-4.png ':size=500')

Indiquer votre login et votre mot de passe puis cliquer sur "Se connecter".

![Se connecter](../img/nextcloud-windows-5.png ':size=500')

## Création de canaux

Si vous souhaitez créer une conversation à plusieurs, il est possible de créer un canal. Pour ce faire, cliquer sur le bouton de création de canal (voir image ci-dessous).

![Créer un canal](../img/nextcloud-create-channel-1.png)

Cliquer ensuite sur "Créer une nouvelle conversation".

![Créer une nouvelle conversation](../img/nextcloud-create-channel-2.png)

Ensuite, vous pouvez entrer le nom du canal, sa description et son image principale. Vous pouvez également régler les paramètres de confidentialité du canal.
- L'option "Permettre aux invités de rejoindre via un lien" permet de générer un lien d'invitation pour le canal et permet à toute personne disposant du lien de lire et écrire dans le canal.
- L'option "Protéger par un mot de passe" permet de protéger le canal par un mot de passe. Les personnes souhaitant rejoindre le canal via le lien devront entrer le mot de passe pour y accéder. Cette option n'est utile que pour les canaux qui permettent aux invités de rejoindre via un lien.
- L'option "Ouvrir la conversation aux utilisateurs enregistrés, en la montrant dans les résultats de recherche" permet de rendre le canal visible dans les résultats de recherche pour les utilisateurs enregistrés. Les utilisateurs enregistrés peuvent rejoindre le canal sans invitation.

![Paramètres de confidentialité et infos du canal](../img/nextcloud-create-channel-3.png)

Vous pouvez ensuite cliquer sur "Ajouter des participants" pour ajouter des participants à votre canal. Ajouter vos participants puis cliquer sur "Créez la conversation".

![Ajouter des participants](../img/nextcloud-create-channel-4.png)

Vous venez de créer votre premier canal. Félicitations !

Si, par la suite, vous souhaitez ajouter des participants à votre canal. Rechercher le nom du participant dans la barre de recherche à droite puis cliquer dessus. Sur le cloud bêta, il faut demander aux utilisateurs de créer un compte à [cette adresse](https://cloud.beta.utc.fr/apps/registration/). Sur le cloud final (à venir), toute personne de l'UTC pourra être trouvée dans la barre de recherche.

![Ajouter des participants](../img/nextcloud-create-channel-5.png)

![Ajouter des participants](../img/nextcloud-create-channel-6.png)

## Technique: Statistiques

Pour réaliser des statistiques sur les messages de Nextcloud Talk, il est possible connaitre, par canal, le dernier message lu par chaque participant. Cela permet de savoir si un message a été lu par tous les participants.

Avant un canal et un message donné, il est donc possible d'obtenir:
- Le nombre de personnes ayant lu le message
- Le nombre total de participants
- Le pourcentage de personnes ayant lu le message
- Le pourcentage de personnes n'ayant pas lu le message
- La répartition des lecteurs et non-lecteurs parmis les groupes LDAP ou le type de population (etu/pers)
- La différence temporelle entre le message et le dernier message lu
- Le Nom, Prénom et l'email de chaque lecteur et non-lecteur

Pour récupérer ces informations, il n'est pas possible d'utiliser les API de nextcloud Talk car elle ne le permettent pas. Au vu des informations nominatives, cela peut-être un choix de conception pour accroitre la confidentialité des données.

> La liste de toutes les routes est disponible à l'aide d'un copier-coller de [ce fichier](https://github.com/nextcloud/spreed/blob/main/openapi-full.json) dans [cet outil](https://editor.swagger.io/).

Il faut alors utiliser directement les requêtes SQL sur la base de données.

Quelques requêtes SQL pour récupérer des statistiques sur les messages de Nextcloud Talk : 

```sql
SELECT *
FROM public.oc_comments
WHERE actor_type = 'users'
  AND message !~ '^\{.*\}$';  -- Exclude messages that start with '{' and end with '}', indicating JSON

-- Sans les messages admin et com
SELECT *
FROM public.oc_comments
WHERE actor_type = 'users'
  AND message !~ '^\{.*\}$'  -- Exclude messages that start with '{' and end with '}', indicating JSON
  AND actor_id NOT IN ('admin', 'com-utc');

-- Compter
SELECT COUNT(*)
FROM public.oc_comments
WHERE actor_type = 'users'
  AND message !~ '^\{.*\}$';  -- Exclude messages that start with '{' and end with '}', indicating JSON

-- Récupérer tout les chat d'une conversation:
SELECT * FROM public.oc_comments
where object_type = 'chat' and object_id = '78' and actor_id!='admin' and message !~ '^\{.*\}$'
ORDER BY id ASC LIMIT 500

-- Récupérer tout les participants d'une conversation (dont le dernier message lu):
SELECT * FROM public.oc_talk_attendees
where room_id='78'
ORDER BY id ASC LIMIT 100

-- Etape 1: Récupérer le timestamp du message cible
WITH target_message AS (
  SELECT creation_timestamp 
  FROM public.oc_comments 
  WHERE id = 4102
),

-- Etape 2: Compter le nombre de messages lus
read_count AS (
  SELECT COUNT(*) AS read_count
  FROM public.oc_talk_attendees AS attendees
  JOIN public.oc_comments AS comments
    ON attendees.room_id = comments.object_id::bigint  -- Convertir object_id en bigint
  WHERE comments.object_type = 'chat'
    AND comments.object_id = '78'
    AND attendees.last_read_message >= comments.id
    AND comments.creation_timestamp >= (SELECT creation_timestamp FROM target_message)
    AND comments.message !~ '^\{.*\}$'  -- Exclure les messages qui commencent par '{' et finissent par '}', indiquant du JSON
),

-- Etape 3: Compter le nombre total de participants
total_count AS (
  SELECT COUNT(*) AS total_count
  FROM public.oc_talk_attendees
  WHERE room_id = '78'
)

-- Etape 4: Récupérer les résultats
SELECT 
  (SELECT read_count FROM read_count),
  (SELECT total_count FROM total_count);

```

Pour le test, j'ai tenté de mettre n8n sur le même réseau que la base de données de nextcloud. Cela n'a pas fonctionné. Il semble que cela créé des conflits entre la base de données de nextcloud et n8n.

Il faudrait donc réaliser un bot nextcloud qui pourrait réaliser ces requêtes SQL en fonction du canal et du message donné par l'utilisateur. Les deux infos étant données dans le lien du message, il suffit de "Copier le lien du message". Il faudra aussi vérifier que l'utilisateur est modérateur du canal pour éviter les abus.

![Copier le lien du message](../img/nextcloud-stats-1.png)


Un exemple d'usage possible:
```bash
@stats https://cloud.beta.utc.fr/call/wqa95v2o#message_5832
```

Pour la réalisation, voici un exemple de bot: https://github.com/cloud-py-api/nc_py_api/tree/main/examples/as_app/talk_bot

Ce bot permet la conversion d'une somme (flottant) avec une devis dans une autre devise. Il réalise un appel API pour obtenir les taux de change en live. Il suffit donc de changer l'appel API par un appel en à posgresql et de changer les inputs et outputs.

Il faut cependant uploader l'image docker du bot sur un registry. Il convient donc de:
  1. Protéger les identifiants de la base de données
  2. Identifier un registry où cette image pourrait être hébergée.